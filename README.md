# ![youtube-logo](.README/youtube-logo.png) YouTube Downloader

A [PySide6] GUI Application for **[yt-dlp]**/**[youtube_dl]**

![preview](.README/preview.png)

## Requirements

- [Python 3.7+] and [pip]
- [FFmpeg for yt-dlp] or [FFmpeg] ([BtbN/FFmpeg-Builds])

## Installation

### with git

Install [git] and run

```bash
git clone https://gitlab.com/Commandcracker/youtube_downloader.git
```

### with zip

Download the [zip] and unzip it with [7-Zip].

### install requirements

```bash
pip install --upgrade -r requirements.txt
```

## Usage

```text
$ python youtube_downloader -h

usage: [-h] [-g GUI] [-f FORMAT] [-u URL] [-p PATH] [-v] [-n] [-t MAXTHREADS]

YouTube Downloader: Download videos from YouTube

optional arguments:
  -h, --help            show this help message and exit
  -g GUI, --gui GUI     0 = GUI, 1 = Terminal GUI, 2 = NO GUI
  -f FORMAT, --format FORMAT
                        video format
  -u URL, --url URL     video URL
  -p PATH, --path PATH  download path
  -v, --verbose         debug info
  -n, --nocolor         no color for terminals that dont support color
  -t MAXTHREADS, --maxthreads MAXTHREADS
                        maximum amount of download threads
```

## Docker

If docker is installed, you can build an image and run this as a container.

```bash
docker build -t youtube_downloader .
```

Once the image is built, youtube_downloader can be used by running the following:

```bash
docker run --rm -t -v <path>:/opt/youtube_downloader/out:rw youtube_downloader -u <url> -f <format>
```

example:

```bash
docker run --rm -t -v $PWD/out:/opt/youtube_downloader/out:rw youtube_downloader -u music -f mp3
```

The optional `--rm` [flag](https://docs.docker.com/engine/reference/run/#clean-up---rm) removes the container file system on completion to prevent cruft build-up.

The optional `-t` [flag](https://docs.docker.com/engine/reference/run/#foreground) allocates a pseudo-TTY which allows colored output.

The optional `-v` [flag](https://docs.docker.com/storage/volumes/) allows saving the video on your file system.

[yt-dlp]: https://github.com/yt-dlp/yt-dlp
[youtube_dl]: https://github.com/ytdl-org/youtube-dl
[PySide6]: https://pypi.org/project/PySide6/
[git]: https://git-scm.com/downloads
[7-Zip]: https://www.7-zip.org/
[FFmpeg]: https://ffmpeg.org/download.html
[BtbN/FFmpeg-Builds]: https://github.com/BtbN/FFmpeg-Builds/releases/tag/latest
[FFmpeg for yt-dlp]: https://github.com/yt-dlp/FFmpeg-Builds/releases/tag/latest
[pip]: https://pip.pypa.io/en/stable/installation/
[Python 3.7+]: https://www.python.org/downloads/
[zip]: https://gitlab.com/Commandcracker/youtube_downloader/-/archive/master/youtube_downloader-master.zip
