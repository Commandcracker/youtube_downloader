from os import getcwd
from os.path import join, exists, isfile
from tempfile import TemporaryDirectory
from shutil import copyfile
from PyInstaller.__main__ import run as pyinstaller_run


def main():
    with TemporaryDirectory() as temp:
        image = join(
            getcwd(),
            "Youtube_Downloader",
            "resources",
            "youtube.ico"
        )
        py_name = '__main__'
        py_path = join(
            getcwd(),
            "Youtube_Downloader",
            py_name + ".py"
        )

        if not (exists(image) and isfile(image)):
            print("image not found")
            exit()

        if not (exists(py_path) and isfile(py_path)):
            print("py-file not found")
            exit()

        new_py = join(temp, py_name + '.py')
        copyfile(py_path, new_py)

        pyinstaller_run([
            "--noconsole",
            "--onefile",
            "--distpath", getcwd(),
            "--specpath", temp,
            "--workpath", temp,
            "--runtime-tmpdir", temp,
            "--icon", image,
            # "--name", "youtube_downloader",
            new_py
        ])


if __name__ == "__main__":
    main()
