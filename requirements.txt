# QT
pyside6~=6.5.2
qt_material~=2.14

# Downloader
yt-dlp>=2023.7.6

# Colors
rich~=13.5.2

# Npyscreen / Curses
npyscreen~=4.10.5
windows-curses~=2.3.1; sys_platform=='win32'

# kill threads
psutil~=5.9.5
