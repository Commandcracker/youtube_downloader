FROM python:3.11.4-alpine3.18

WORKDIR /opt/youtube_downloader

ADD requirements.txt .

RUN set -eux; \
    apk add --no-cache --update ffmpeg; \
    sed -i '/pyside6/d' requirements.txt; \
    sed -i '/psutil/d' requirements.txt; \
    pip install --no-cache-dir -r requirements.txt; \
    # Cleanup
    pip uninstall pip -y; \
    rm requirements.txt

ADD youtube_downloader ./youtube_downloader

ENTRYPOINT ["python", "youtube_downloader", "--gui", "2", "--path", "./out"]
