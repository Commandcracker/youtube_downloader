# local modules
from Logger import Logger


class DowloadCallback:
    def __init__(self, logger: Logger, _callback):
        self.logger = logger
        self._callback = _callback

    def callback(self, successfull: bool):
        if successfull:
            self.logger.info("Successfully Downloaded!", "green")
        else:
            self.logger.error("Faild to Download!")
        self._callback()
