# built-in modules
from os.path import isfile, exists, normpath, isdir

# local modules
from Formats import Formats


class Checks():
    """ download check function's """

    def __init__(self, formats: Formats):
        self.formats = formats

    def urls(self, path):
        if path.endswith(".urls"):
            if exists(path) and isfile(path):
                return True  # is a urls file
            return '"' + path + '" is not a valid .urls file!'
        return False  # isn't a urls file

    def path(self, path):
        if not (path is None):
            path = normpath(path)
            if exists(path) and isdir(path):
                return True  # valid path
            return '"' + path + '" is not a valid path!'
        return "You need to enter a path!"

    def format(self, format):
        if not (format is None):
            if self.formats.formats.get(format) is None:
                return "Format not avaliable!"
            return True  # valid format
        return "You need to select a format!"

    def url(self, url):
        if not (url is None or url == "" or url.isspace()):
            return True  # valid url
        return "You must enter a valid url!"

    def all(self, url, path, format):
        output = self.url(url)
        if isinstance(output, str):
            return output

        output = self.urls(url)
        if isinstance(output, str):
            return output
        if output:
            return False  # urls file

        output = self.path(path)
        if isinstance(output, str):
            return output

        output = self.format(format)
        if isinstance(output, str):
            return output

        return True  # normal url with path, format
