# built-in modules
from threading import Thread
from os.path import normpath, join
from time import perf_counter

# pip modules

try:
    from yt_dlp import YoutubeDL
    from yt_dlp.postprocessor.ffmpeg import FFmpegMetadataPP
    print("Using yt_dlp")
    yt_dlp = True
except Exception:
    from youtube_dl import YoutubeDL
    from youtube_dl.postprocessor.ffmpeg import FFmpegMetadataPP
    print("Using youtube_dl")
    yt_dlp = False

try:
    from PySide6.QtGui import QColor
except Exception:
    def QColor(x,y,z):
        pass

# local modules
from Logger import Logger
from QueueManager import QueueManager
from Formats import Formats
from Hook import Hook
from YTLogger import YTLogger


class Download(Thread):
    """ download class """

    def __init__(
        self,
        url: str,
        format: str,
        path: str,
        logger: Logger,
        queue_manager: QueueManager,
        formats: Formats,
        callback=None,
        tree_item = None, # QTreeWidgetItem
        threads_lable = None # QLabel
    ):
        Thread.__init__(self)

        self.queue_manager = queue_manager
        self.queue_manager.thread_counter += 1
        self.thread = self.queue_manager.thread_counter

        self.threads_lable = threads_lable

        self.url = url
        self.path = path

        self.logger = logger

        # format
        self.format = format
        self.formats = formats
        self.format_json = formats.formats[self.format]
        self.format_json["outtmpl"] = join(
            normpath(self.path), "%(title)s.%(ext)s")
        self.format_json["format"] = self.format_json["format"] + "/best"
        # logger / hooks
        self.hook = Hook(tree_item, self.logger)
        self.format_json["progress_hooks"] = [self.hook.my_hook]
        self.format_json["logger"] = YTLogger(self.logger, tree_item)
        self.format_json["default_search"] = "auto"

        self.tree_item = tree_item

        self.callback = callback

        self.successfull = True
        self.color = QColor(70, 70, 70)

    def run(self):
        start = perf_counter()

        if self.threads_lable:
            self.threads_lable.setText(
                f"Threads: {self.queue_manager.thread_counter}/"
                f"{self.queue_manager.max_threads}"
            )

        self.logger.debug(
            f"Starting thread {self.thread} "
            f"url={self.url} "
            f"format={self.format} "
            f"path={self.path}"
        )

        with YoutubeDL(self.format_json) as ydl:
            ydl.add_post_processor(FFmpegMetadataPP(
                ydl) if yt_dlp else FFmpegMetadataPP())
            js = ydl.extract_info(self.url, download=False, process=True)

            if js:
                if "entries" in js:
                    for part in js['entries']:
                        self.queue_manager.queue.insert(
                            len(self.queue_manager.queue),
                            [
                                part["id"],
                                self.format,
                                self.path
                            ]
                        )
                else:
                    error_code = ydl.download([self.url])
                    if error_code != 0 and self.tree_item:
                        self.successfull = False
                        self.color = QColor(255, 0, 0)
            else:
                if self.tree_item:
                    self.successfull = False
                    self.color = QColor(255, 0, 0)

        self.queue_manager.thread_counter -= 1

        if self.threads_lable:
            self.threads_lable.setText(
                f"Threads: {self.queue_manager.thread_counter}/"
                f"{self.queue_manager.max_threads}"
            )

        self.logger.debug(
            f"Thread {self.thread} took {round(perf_counter() - start)}s, "
            f"{self.queue_manager.thread_counter} threads remaining."
        )

        if self.tree_item:
            for i in range(0, self.tree_item.columnCount()):
                self.tree_item.setForeground(i, self.color)

        if self.callback:
            self.callback(self.successfull)
