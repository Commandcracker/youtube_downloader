import os
import re


def join_html_text_by_dict(dict: dict) -> str:
    text = ""
    for i in dict:
        text = text + str(i) + "<br>"
    return text


ansi_escape_codes = re.compile(r'''
    \x1B  # ESC
    (?:   # 7-bit C1 Fe (except CSI)
        [@-Z\\-_]
    |     # or [ for CSI, followed by a control sequence
        \[
        [0-?]*  # Parameter bytes
        [ -/]*  # Intermediate bytes
        [@-~]   # Final byte
    )
''', re.VERBOSE)

def remove_ansi_escape_codes(text: str) -> str:
    # Remove all Ansi Escape codes
    # 7-bit C1 ANSI sequences
    return ansi_escape_codes.sub('', text)


def text_to_html(text: str, color: str = None) -> str:
    # Line Endings
    # \n = LF (Line Feed) -> Moves the cursor down to the next line without returning to the beginning of the line.
    text = text.replace("\n", "<br>")
    # \r = CR (Carriage Return) -> Moves the cursor to the beginning of the line without advancing to the next line
    text = text.replace("\r", "")

    # Remove all Ansi Escape codes
    text = remove_ansi_escape_codes(text)

    # Escape Space
    text = text.replace("\s", "&nbsp;")
    # Escape Tabs (replace with 4 spaces)
    text = text.replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;")

    if color:
        text = "<font color='" + color + "'>" + text + "</font>"

    # return
    return text


def get_download_path() -> str:
    if os.name == 'nt':
        import winreg
        sub_key = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders'
        downloads_guid = '{374DE290-123F-4565-9164-39C4925E467B}'
        with winreg.OpenKey(winreg.HKEY_CURRENT_USER, sub_key) as key:
            location = winreg.QueryValueEx(key, downloads_guid)[0]
        return location
    
    home = os.path.expanduser('~')
    user_dirs_path = os.path.join(home, '.config', "user-dirs.dirs")
    if os.path.exists(user_dirs_path):
        with open(os.path.join(home, '.config', "user-dirs.dirs")) as file:
            for line in file.readlines():
                var = line.split("=")
                if var[0] == "XDG_DOWNLOAD_DIR":
                    return var[1].rstrip("\n").replace('"', '').replace("'", '').replace("$HOME", home)
    guess_paths = [
        os.path.join(home, 'downloads'),
        os.path.join(home, 'Downloads')
    ]
    for path in guess_paths:
        if os.path.exists(path):
            return path
    return home
