#!/usr/bin/python3
# -*- coding: utf-8 -*-

# built-in modules
from argparse import ArgumentParser
from os import getpid, environ
from signal import signal, SIGINT, SIG_DFL
from sys import argv, platform

# pip modules
from rich.traceback import install

# local modules
from Logger import Logger
from QueueManager import QueueManager
from Checks import Checks
from YoutubeDownloader import YoutubeDownloader
from Formats import Formats
from QueueUpdater import QueueUpdater
from utils import get_download_path

def main():
    # argument options #
    parser = ArgumentParser(
        description="YouTube Downloader: Download videos from YouTube")
    parser.add_argument('-g', '--gui', type=int, help="0 = GUI, "
                                                      "1 = Terminal GUI, "
                                                      "2 = NO GUI", default=0)
    parser.add_argument('-f', '--format', type=str, help="video format")
    parser.add_argument('-u', '--url', type=str, help="video URL")
    parser.add_argument('-p', '--path', type=str, help="download path")
    parser.add_argument('-v', '--verbose',
                        help="debug info", action="store_true")
    parser.add_argument('-n', '--nocolor', help="no color for terminals that dont support color",
                        action="store_true")
    parser.add_argument('-t', '--maxthreads', type=int,
                        help="maximum amount of download threads", default=5)
    args = parser.parse_args()

    if args.nocolor:
        # https://rich.readthedocs.io/en/stable/console.html#environment-variables
        environ["NO_COLOR"] = ""
    install()

    # make qt and Threads CTRL+C Interruptable
    # https://stackoverflow.com/questions/5160577/ctrl-c-doesnt-work-with-pyqt#5160720
    if platform != "win32":
        signal(SIGINT, SIG_DFL)

    if not args.path:
        args.path = get_download_path()

    logger = Logger(
        not bool(args.nocolor),
        bool(args.verbose)
    )

    formats = Formats(args)
    checks = Checks(formats)

    queue_manager = QueueManager(int(args.maxthreads))
    queueupdater = QueueUpdater(queue_manager, formats, logger)

    youtube_downloader = YoutubeDownloader(
        formats,
        logger,
        checks,
        queue_manager,
        queueupdater
    )

    # QT menu
    if args.gui is None or args.gui == 0:
        # pip modules
        from PySide6.QtWidgets import QApplication
        from psutil import Process
        from qt_material import apply_stylesheet

        from QTGUI import QTGUI

        app = QApplication(argv)
        # Theme
        apply_stylesheet(app, theme='dark_red.xml')
        # Name
        app.setApplicationName("Youtube_Downloader")
        # Show main window
        window_manager = QTGUI(
            formats=formats,
            youtubedownloader=youtube_downloader,
            checks=checks,
            logger=logger,
            queue_manager=queue_manager,
            url=args.url,
            format=args.format,
            path=args.path
        )
        window_manager.loadUis()
        queueupdater.tree_widget = window_manager.youtube_ui.treeWidget
        queueupdater.threads_lable = window_manager.youtube_ui.Label_Threads

        window_manager.show_youtube_ui()

        try:
            app.exec()
            # kill whole process to terminate threads
            Process(getpid()).kill()
        except KeyboardInterrupt:
            # kill whole process to terminate threads
            Process(getpid()).kill()

    # Npyscreen menu
    elif args.gui == 1:
        from npyscreen_menu import npyscreen_menu
        try:
            npyscreen_menu(
                logger,
                formats,
                youtube_downloader,
                checks,
                not bool(args.nocolor),
                args.url,
                args.format,
                args.path
            ).run()
        except KeyboardInterrupt:
            exit()

    # Terminal cli
    elif args.gui == 2:
        output = checks.all(args.url, args.path, args.format)
        if isinstance(output, str):
            logger.error(str(output))
            exit(1)
        else:
            if output:
                youtube_downloader.download(
                    args.url,
                    args.format,
                    args.path
                )

            else:
                youtube_downloader.download_urls(args.url)
    else:
        logger.error("gui " + str(args.gui) + " is not an option")
        exit(1)

if __name__ == '__main__':
    main()
