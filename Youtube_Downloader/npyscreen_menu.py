# built-in modules
from npyscreen import NPSApp, setTheme, Form, TitleText, TitleSelectOne
from npyscreen.npysThemeManagers import ThemeManager

# local modules
from YoutubeDownloader import YoutubeDownloader
from Checks import Checks
from Formats import Formats
from Logger import Logger


class npyscreen_menu(NPSApp):
    """
        npyscreen menu and Theam's
    """

    def __init__(self, logger: Logger, formats: Formats, youtube_downloader: YoutubeDownloader, checks: Checks, colord: bool, url: None = str, format: None = str, path: None = str):
        self.url = url
        self.format = format
        self.formats = formats
        self.path = path
        self.youtube_downloader = youtube_downloader
        self.checks = checks
        self.colord = colord
        self.logger = logger

    class NoColorTheme(ThemeManager):
        default_colors = {
            'DEFAULT': 'WHITE_BLACK',
            'FORMDEFAULT': 'WHITE_BLACK',
            'NO_EDIT': 'WHITE_BLACK',
            'STANDOUT': 'WHITE_BLACK',
            'CURSOR': 'WHITE_BLACK',
            'CURSOR_INVERSE': 'BLACK_WHITE',
            'LABEL': 'WHITE_BLACK',
            'LABELBOLD': 'WHITE_BLACK',
            'CONTROL': 'WHITE_BLACK',
            'WARNING': 'WHITE_BLACK',
            'CRITICAL': 'WHITE_BLACK',
            'GOOD': 'WHITE_BLACK',
            'GOODHL': 'WHITE_BLACK',
            'VERYGOOD': 'WHITE_BLACK',
            'CAUTION': 'WHITE_BLACK',
            'CAUTIONHL': 'WHITE_BLACK',
        }

    class ColordTheme(ThemeManager):
        default_colors = {
            'DEFAULT': 'WHITE_BLACK',
            'FORMDEFAULT': 'RED_BLACK',
            'NO_EDIT': 'BLUE_BLACK',
            'STANDOUT': 'CYAN_BLACK',
            'CURSOR': 'CYAN_BLACK',
            'CURSOR_INVERSE': 'BLACK_CYAN',
            'LABEL': 'GREEN_BLACK',
            'LABELBOLD': 'WHITE_BLACK',
            'CONTROL': 'YELLOW_BLACK',
            'WARNING': 'RED_BLACK',
            'CRITICAL': 'BLACK_RED',
            'GOOD': 'GREEN_BLACK',
            'GOODHL': 'GREEN_BLACK',
            'VERYGOOD': 'BLACK_GREEN',
            'CAUTION': 'YELLOW_BLACK',
            'CAUTIONHL': 'BLACK_YELLOW',
        }

    def main(self):
        if self.colord:
            setTheme(npyscreen_menu.ColordTheme)
        else:
            setTheme(npyscreen_menu.NoColorTheme)
        F = Form(name="Youtube Downloader")
        t1 = F.add(TitleText, name="Url:", value=self.url)
        t2 = F.add(TitleText, name="Path:", value=self.path)
        fromats_list = []
        for format in self.formats.formats:
            fromats_list.append(format)

        def update_format():
            if self.format:
                return fromats_list.index(self.format)
            else:
                return None

        ms = F.add(
            TitleSelectOne,
            name="Format",
            values=fromats_list,
            value=update_format()
        )

        F.edit()
        self.url = t1.value
        self.path = t2.value
        try:
            self.format = ms.get_selected_objects()[0]
        except IndexError:
            pass

        output = self.checks.all(self.url, self.path, self.format)
        if isinstance(output, str):
            self.logger.error(str(output))
        else:
            if output:
                # normal url with path, format
                self.youtube_downloader.download(
                    self.url,
                    self.format,
                    self.path
                )

            else:
                self.youtube_downloader.download_urls(self.url)
