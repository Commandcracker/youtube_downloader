# local modules
#from PySide6.QtWidgets import QTreeWidgetItem
from Logger import Logger
from utils import text_to_html


class YTLogger(object):
    def __init__(
        self,
        logger: Logger,
        tree_item = None # QTreeWidgetItem
    ):
        self.logger = logger
        self.tree_item = tree_item
        if self.tree_item:
            self.tree_item.logs = []

    def debug(self, msg):
        self.logger.ytdl(msg)
        if self.tree_item:
            self.tree_item.logs.append(
                text_to_html(msg, "white")
            )

    def warning(self, msg):
        self.logger.ytdl("WARNING: " + msg, "yellow")
        if self.tree_item:
            self.tree_item.logs.append(
                text_to_html("WARNING: " + msg, "orange")
            )

    def error(self, msg):
        self.logger.ytdl(msg, "red")
        if self.tree_item:
            self.tree_item.logs.append(
                text_to_html(msg, "red")
            )
