# local modules
from QueueManager import QueueManager
from Logger import Logger
from Checks import Checks
from Download import Download
from Formats import Formats
from QueueUpdater import QueueUpdater
from DowloadCallback import DowloadCallback

# pip modules
#from PySide6.QtWidgets import QLabel, QTreeWidgetItem


class YoutubeDownloader:
    def __init__(
        self,
        formats: Formats,
        logger: Logger,
        checks: Checks,
        queue_manager: QueueManager,
        queue_updater: QueueUpdater
    ):
        self.logger = logger
        self.formats = formats
        self.checks = checks
        self.queue_manager = queue_manager
        self.queue_updater = queue_updater

    def download(
        self,
        url: str,
        format: str,
        path: str,
        tree_item = None, # QTreeWidgetItem
        threads_label = None # QLabel
    ):
        if self.queue_manager.thread_counter != self.queue_manager.max_threads:
            Download(
                url,
                format,
                path,
                self.logger,
                self.queue_manager,
                self.formats,
                DowloadCallback(
                    self.logger,
                    self.queue_updater.update
                ).callback,
                tree_item,
                threads_label
            ).start()
        else:
            self.queue_manager.queue.insert(
                len(self.queue_manager.queue),
                [
                    url,
                    format,
                    path
                ]
            )

    def download_urls(self, path):
        """ download urls function """
        with open(path, 'r') as url_file:
            urls = url_file.readlines()
            for url_index in range(0, len(urls)):
                # remove \n from urls #
                urls[url_index] = urls[url_index].split('\n')[0]
            for u in urls:
                # if not url is nothing and ignore #
                if not (u == "" or u.isspace() or u.startswith("#")):
                    foramt_url = u.split(" ", 2)
                    self.logger.debug("Processing " + str(foramt_url))
                    if len(foramt_url) == 1:
                        url = foramt_url[0]
                        format = "mp4"
                        path = ".."
                    if len(foramt_url) == 2:
                        url = foramt_url[1]
                        format = foramt_url[0]
                        path = ".."
                    if len(foramt_url) == 3:
                        url = foramt_url[1]
                        format = foramt_url[0]
                        path = foramt_url[2]
                    error = self.checks.format(format)
                    if isinstance(error, str):
                        self.logger.error(error)
                    else:
                        error = self.checks.path(path)
                        if isinstance(error, str):
                            self.logger.error(error)
                        else:
                            self.download(
                                url,
                                format,
                                path
                            )
