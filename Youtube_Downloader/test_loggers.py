# local modules
from Logger import Logger
from YTLogger import YTLogger
from rich.traceback import install


def main():
    install()

    logger = Logger(True, True)
    test_message = "TEST"
    logger.info(test_message)

    logger.debug(test_message)
    logger.error(test_message)
    logger.warning(test_message)

    logger.ytdl(test_message)

    # YTLogger
    yt_logger = YTLogger(logger)

    yt_logger.debug(test_message)
    yt_logger.warning(test_message)
    yt_logger.error(test_message)

if __name__ == "__main__":
    main()
