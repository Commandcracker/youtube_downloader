# pip modules
#from PySide6.QtWidgets import QTreeWidgetItem

# local modules
from Logger import Logger


class Hook(object):
    def __init__(
        self,
        tree_item, # QTreeWidgetItem
        logger: Logger
    ):
        self.tree_item = tree_item
        self.logger = logger

    def my_hook(self, d):
        if self.tree_item:
            self.tree_item.setData(3, 0, d['status'])

            if '_eta_str' in d:
                self.tree_item.setData(4, 0, d['_eta_str'])
            if '_percent_str' in d:
                self.tree_item.setData(5, 0, d['_percent_str'])
            if '_speed_str' in d:
                self.tree_item.setData(6, 0, d['_speed_str'])
            if '_total_bytes_str' in d:
                self.tree_item.setData(7, 0, d['_total_bytes_str'])
