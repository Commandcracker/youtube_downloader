# built-in modules
from os.path import join, dirname
from json import load


class Formats:
    def __init__(self, args):
        self.formats = load(
            open(join(dirname(__file__), "resources", "formats.json"), 'r'))

        for key in self.formats:
            self.formats[key]["sleep_interval"] = 0
            self.formats[key]["max_sleep_interval"] = 0
            self.formats[key]["nooverwrites"] = True
            self.formats[key]["no_warnings"] = True
            self.formats[key]["ignoreerrors"] = True
            self.formats[key]["nocheckcertificate"] = True

            # Playlist
            self.formats[key]["playlistreverse"] = True
            self.formats[key]['extract_flat'] = 'in_playlist'

            # color
            self.formats[key]["no_color"] = True

            # debug
            if args.verbose:
                self.formats[key]["verbose"] = True
