# built-in modules
from PySide6.QtUiTools import QUiLoader
from PySide6.QtGui import QIcon
from PySide6.QtCore import QFile, Qt
from PySide6.QtWidgets import (
    QTreeWidgetItem,
    QFileDialog,
    QWidget,
    QMenu
)
from os.path import join, dirname

# local modules
from Logger import Logger
from YoutubeDownloader import YoutubeDownloader
from Checks import Checks
from QueueManager import QueueManager
from Formats import Formats
from utils import join_html_text_by_dict


class QTGUI(object):
    """
        PYQT5
    """

    def __init__(
        self,
        formats: Formats,
        youtubedownloader: YoutubeDownloader,
        checks: Checks,
        logger: Logger,
        queue_manager: QueueManager,
        url: None = str,
        format: None = str,
        path: None = str
    ):
        self.url = url
        self.format = format
        self.formats = formats
        self.path = path
        self.youtubedownloader = youtubedownloader
        self.logger = logger
        self.checks = checks
        self.queue_manager = queue_manager

        self.youtube_ui: QWidget
        self.log_ui: QWidget
        self.error_ui: QWidget

    def get_resources(self, file):
        return join(dirname(__file__), "resources", file)

    def loadUi(self, uifilename: str, parent=None):
        loader = QUiLoader()
        uifile = QFile(self.get_resources(uifilename))
        uifile.open(QFile.ReadOnly)
        ui = loader.load(uifile, parent)
        uifile.close()
        return ui

    def loadUis(self):
        self.youtube_ui = self.setup_youtube_ui()
        self.log_ui = self.setup_log_ui()
        self.error_ui = self.setup_error_ui()

    def setup_youtube_ui(self):
        window = self.loadUi("YouTube_Downloader.ui")

        window.Download_Button.clicked.connect(self.download)

        for format in self.formats.formats:
            window.ComboBox_Video.addItem(format)

        window.ComboBox_Video.currentTextChanged.connect(self.format_change)
        window.ComboBox_Video.setCurrentText(self.format)

        window.LineEdit_Url.textChanged.connect(
            lambda: self.url_change(window.LineEdit_Url.text())
        )
        window.LineEdit_Url.setText(self.url)
        window.LineEdit_Path.textChanged.connect(
            lambda: self.path_change(window.LineEdit_Path.text())
        )
        window.LineEdit_Path.setText(self.path)

        window.setWindowIcon(QIcon(self.get_resources("youtube.ico")))
        window.setObjectName("YouTube Downloader")

        window.Browse_Button.clicked.connect(
            lambda: self.set_destination(window)
        )

        def openMenu(position):
            item = window.treeWidget.currentItem()
            # If item exists and if item is finished
            if item and (item.foreground(0).color().getRgb()[0] == 70 or item.foreground(0).color().getRgb()[0] == 255):
                menu = QMenu()
                menu.addAction('Delete')
                menu.addAction('Logs')
                a = menu.exec(
                    window.treeWidget.viewport().mapToGlobal(position)
                )
                if a:
                    if a.text() == 'Delete':
                        window.treeWidget.takeTopLevelItem(
                            window.treeWidget.currentIndex().row()
                        )
                    elif a.text() == 'Logs':
                        self.show_log_ui(join_html_text_by_dict(item.logs))

        window.treeWidget.setContextMenuPolicy(Qt.CustomContextMenu)
        window.treeWidget.customContextMenuRequested.connect(openMenu)

        window.treeWidget.setColumnWidth(0, 250)
        window.treeWidget.setColumnWidth(1, 90)
        window.treeWidget.setColumnWidth(2, 250)
        window.treeWidget.setColumnWidth(3, 90)
        window.treeWidget.setColumnWidth(4, 65)
        window.treeWidget.setColumnWidth(5, 100)
        window.treeWidget.setColumnWidth(6, 80)

        # Remove branch icon
        window.treeWidget.setIndentation(0)

        window.Label_Threads.setText(
            "Threads: " +
            str(self.queue_manager.thread_counter) +
            "/" + str(self.queue_manager.max_threads)
        )

        return window

    def setup_error_ui(self):
        window = self.loadUi("Error.ui")

        window.setWindowIcon(QIcon(self.get_resources("error.ico")))
        window.setObjectName("Error")

        window.setFixedSize(window.width(), window.height())

        window.setWindowFlags(Qt.WindowCloseButtonHint)

        return window

    def setup_log_ui(self):
        window = self.loadUi("Log.ui")

        window.setWindowIcon(QIcon(self.get_resources("Log.ico")))
        window.setObjectName("Log")

        window.setWindowFlags(Qt.WindowCloseButtonHint)

        return window

    def show_youtube_ui(self):
        qtRectangle = self.youtube_ui.frameGeometry()
        qtRectangle.moveCenter(
            self.youtube_ui.screen().availableGeometry().center()
        )
        self.youtube_ui.move(qtRectangle.topLeft())
        self.youtube_ui.show()

    def show_error_ui(self, text):
        self.logger.error(text)
        self.error_ui.Text_Browser.setHtml(text)
        self.error_ui.activateWindow()

        size = self.youtube_ui.size()
        pos = self.youtube_ui.pos()
        ssize = self.error_ui.size()

        x = pos.x() + size.width()/2 - ssize.width()/2
        y = pos.y() + size.height()/2 - ssize.height()/2

        self.error_ui.move(x, y)

        self.error_ui.show()

    def show_log_ui(self, text):
        self.log_ui.Text_Browser.setHtml(text)
        self.log_ui.activateWindow()

        self.log_ui.resize(800, 242)

        size = self.youtube_ui.size()
        pos = self.youtube_ui.pos()
        ssize = self.log_ui.size()

        x = pos.x() + size.width()/2 - ssize.width()/2
        y = pos.y() + size.height()/2 - ssize.height()/2

        self.log_ui.move(x, y)

        self.log_ui.show()

    def path_change(self, path):
        self.path = path
        self.logger.debug("path=" + self.path)

    def url_change(self, url):
        self.url = url
        self.logger.debug("url=" + self.url)

    def set_destination(self, window: QWidget):
        file_name = QFileDialog.getExistingDirectory(
            window, "Select Directory",
            self.path,
            QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks
        )
        if file_name != '':
            window.LineEdit_Path.setText(file_name)

    def download(self):
        output = self.checks.all(self.url, self.path, self.format)
        if isinstance(output, str):
            self.show_error_ui(output)
        else:
            if output:
                # normal url with path, format

                tree_item = QTreeWidgetItem([
                    self.url,
                    self.format,
                    self.path
                ])

                self.youtube_ui.treeWidget.addTopLevelItem(tree_item)

                self.youtubedownloader.download(
                    self.url,
                    self.format,
                    self.path,
                    tree_item,
                    self.youtube_ui.Label_Threads
                )

            else:
                self.youtubedownloader.download_urls(self.url)

    def format_change(self, format):
        self.format = format
        self.logger.debug("format=" + self.format)
