# built-in modules
from time import strftime

# pip modules
from rich.console import Console
from rich.text import Text
from rich.highlighter import ReprHighlighter

# local modules
from utils import remove_ansi_escape_codes


class Logger:
    def __init__(self, colored: bool = False, print_debug: bool = False):
        self.colored = colored
        self.print_debug = print_debug
        self.console = Console()

    def log(self, message: str, prefix: str, style: str = None):
        time_hms = str(strftime("%H")) + ":" + \
            str(strftime("%M")) + ":" + str(strftime("%S"))

        prefix = '[' + time_hms + " " + str(prefix) + "]: "

        message = (remove_ansi_escape_codes(message[0]), message[1])

        if self.colored and style:
            self.console.print(
                ReprHighlighter().__call__(Text.assemble(
                    prefix,
                    message
                )),
                style=style
            )
        else:
            self.console.print(message[0], highlight=False)

    def info(self, message: str, style: str = None):
        self.log((message, style), "INFO", "white")

    def debug(self, message: str, style: str = None):
        if self.print_debug:
            self.log((message, style), "DEBUG", "bright_black")

    def error(self, message: str, style: str = None):
        self.log((message, style), "ERROR", "red")

    def warning(self, message, style: str = None):
        self.log((message, style), "WARNING", "yellow")

    def ytdl(self, message: str, style: str = None):
        self.log((message, style), "YTDL", "magenta")
