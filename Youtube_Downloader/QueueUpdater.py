# pip modules
#from PySide6.QtWidgets import QLabel, QTreeWidget, QTreeWidgetItem

# local modules
from Logger import Logger
from Formats import Formats
from Download import Download
from QueueManager import QueueManager
from DowloadCallback import DowloadCallback


class QueueUpdater(object):

    def __init__(
        self,
        queue_manager: QueueManager,
        formats: Formats,
        logger: Logger,
        tree_widget = None, # QTreeWidget
        threads_lable = None # QLabel
    ):
        self.logger = logger
        self.formats = formats
        self.queue_manager = queue_manager
        self.tree_widget = tree_widget
        self.threads_lable = threads_lable

    def update(self):
        if self.queue_manager.thread_counter != self.queue_manager.max_threads:
            try:
                if self.queue_manager.queue[0] is not None:
                    tree_item = None

                    if self.tree_widget:
                        from PySide6.QtWidgets import QTreeWidgetItem
                        tree_item = QTreeWidgetItem([
                            self.queue_manager.queue[0][0],
                            self.queue_manager.queue[0][1],
                            self.queue_manager.queue[0][2],
                        ])

                        self.tree_widget.addTopLevelItem(tree_item)

                    Download(
                        url=self.queue_manager.queue[0][0],
                        format=self.queue_manager.queue[0][1],
                        path=self.queue_manager.queue[0][2],
                        logger=self.logger,
                        queue_manager=self.queue_manager,
                        formats=self.formats,
                        callback=DowloadCallback(
                            self.logger,
                            self.update
                        ).callback,
                        tree_item=tree_item,
                        threads_lable=self.threads_lable
                    ).start()

                    self.queue_manager.queue.remove(
                        self.queue_manager.queue[0])
                    self.logger.debug(
                        "Threads: " +
                        str(self.queue_manager.thread_counter) +
                        " / " +
                        str(self.queue_manager.max_threads)
                    )
                    self.update()
            except IndexError:
                pass
