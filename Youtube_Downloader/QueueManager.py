
class QueueManager(object):
    """ queue manager class """

    def __init__(self, max_threads: int = 5):
        self.queue = []
        self.max_threads = max_threads
        self.thread_counter = 0
